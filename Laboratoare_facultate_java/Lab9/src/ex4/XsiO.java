package ex4;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class XsiO extends JFrame {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        static JButton[] buttons = new JButton[9];
        public static int count = 0;
        private static int[][] check = new int[][] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 }, { 1, 4, 7 },
                { 2, 5, 8 }, { 0, 4, 8 }, { 2, 4, 6 } };

        static JButton newGame;
        static JLabel drawLabel;
        static JTextField drawField;
        public static int drawCount = 0;

        static JLabel winLabelX;
        static JTextField winFieldX;
        public static int winX = 0;

        static JLabel winLabelO;
        static JTextField winFieldO;
        public static int winO = 0;

        private static void Game() {
            JFrame frame = new JFrame("Tic Tac Toe");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLayout(new BorderLayout());

            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(3, 3));

            for (int i = 0; i < 9; i++) {
                buttons[i] = new Button();
                panel.add(buttons[i]);
            }

            frame.getContentPane().add(panel, BorderLayout.CENTER);

            SpringLayout layout = new SpringLayout();

            JPanel menu = new JPanel();
            menu.setLayout(layout);
            menu.setPreferredSize(new Dimension(100, 100));
            frame.getContentPane().add(menu, BorderLayout.EAST);

            newGame = new JButton("New Game");
            newGame.addActionListener(new newGame());

            winLabelX = new JLabel("X wins:");
            winFieldX = new JTextField("0", 5);
            winFieldX.setEditable(false);

            winLabelO = new JLabel("O wins:");
            winFieldO = new JTextField("0", 5);
            winFieldO.setEditable(false);

            drawLabel = new JLabel("Draws:");
            drawField = new JTextField("0", 5);
            drawField.setEditable(false);

            menu.add(newGame);
            menu.add(winLabelX);
            menu.add(winFieldX);
            menu.add(winLabelO);
            menu.add(winFieldO);
            menu.add(drawLabel);
            menu.add(drawField);

            layout.putConstraint(SpringLayout.NORTH, newGame, 5, SpringLayout.WEST, frame);
            layout.putConstraint(SpringLayout.SOUTH, winLabelX, 30, SpringLayout.SOUTH, newGame);
            layout.putConstraint(SpringLayout.SOUTH, winFieldX, 30, SpringLayout.SOUTH, winLabelX);
            layout.putConstraint(SpringLayout.SOUTH, winLabelO, 30, SpringLayout.SOUTH, winFieldX);
            layout.putConstraint(SpringLayout.SOUTH, winFieldO, 30, SpringLayout.SOUTH, winLabelO);
            layout.putConstraint(SpringLayout.SOUTH, drawLabel, 30, SpringLayout.SOUTH, winFieldO);
            layout.putConstraint(SpringLayout.SOUTH, drawField, 30, SpringLayout.SOUTH, drawLabel);

            frame.pack();

            frame.setSize(500, 400);
            frame.setResizable(false);
            frame.setVisible(true);
        }

        public static void main(String[] args) {
            Game();
        }

        public static class newGame implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        }

        private static class Button extends JButton implements ActionListener {
            boolean win;
            String letter = "";
            String X, O;
            int option;

            public Button() {
                super();
                letter = "";
                X = "X";
                O = "O";
                setText(letter);
                this.addActionListener(this);
            }

            public void actionPerformed(ActionEvent e) {

                if (((count % 2) == 0) && (getText().equals("")) && (win == false)) {
                    letter = X;
                    count++;
                } else if (((count % 2) == 1) && (getText().equals("")) && (win == false)) {
                    letter = O;
                    count++;
                }

                setText(letter);

                for (int i = 0; i < 8; i++) {
                    if (buttons[check[i][0]].getText().equals(buttons[check[i][1]].getText())
                            && buttons[check[i][1]].getText().equals(buttons[check[i][2]].getText())
                            && buttons[check[i][0]].getText() != "") {
                        win = true;
                    }
                }

                if (win == true) {
                    option = JOptionPane.showConfirmDialog(null, letter + " wins! Play again?",
                            letter + " wins!", JOptionPane.YES_NO_OPTION);
                    if (letter == X) {
                        winX++;
                      XsiO.winFieldX.setText(String.valueOf(winX));

                    } else {
                        winO++;
                   XsiO.winFieldO.setText(String.valueOf(winO));

                    }

                } else if (count == 9 && win == false) {
                    option = JOptionPane.showConfirmDialog(null, "Draw Game! Play again?", "It's a tie!",
                            JOptionPane.YES_NO_OPTION);
                    win = true;
                    drawCount++;
                  XsiO.drawField.setText(String.valueOf(drawCount));
                }

                if (option == JOptionPane.YES_OPTION && win == true) {
                    reset();
                    win = false;

                } else if (option == JOptionPane.NO_OPTION) {
                    System.exit(0);
                }
            }

        }

        public static void reset() {
            for (int i = 0; i < 9; i++) {
                buttons[i].setText("");
            }
            count = 0;
        }
    }