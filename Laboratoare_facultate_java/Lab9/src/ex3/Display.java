package ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Display extends JFrame {

    static JTextField needText;
    JButton but;
    static String filetos = "data.txt";

    static void citire() throws Exception{
        String text="";
        File file = new File(filetos);

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            text = text+ " " + st;
            System.out.println(text);
        }
        needText.setText(text);

    }

    Display(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);

        this.setLayout(null);

        needText = new JTextField(" ");
        needText.setBounds(10, 50, 100, 100);

        but = new JButton("Check");
        but.setBounds(10,150,100, 100);
        add(needText);add(but);
        setVisible(true);

        but.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String textCitit = needText.getText();
                if(textCitit.equals("test.txt")) {
                    try {
                        citire();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                else {
                    System.out.println("Eroare");
                }
            }
        });
    }


    public static void main(String[] args) {
        Display d = new Display();

    }
}
