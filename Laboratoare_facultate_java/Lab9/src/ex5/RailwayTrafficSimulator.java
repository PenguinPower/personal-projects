package ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class RailwayTrafficSimulator extends JFrame {
    JButton addTrain, displaySeg,addstationAndsegments;
    JTextField train,currentSeg,stationName;
    JTextArea segmentdetails, isadded;


    ArrayList<Station> stations = new ArrayList<Station>();
    HashMap trains = new HashMap();

    public RailwayTrafficSimulator(){
        setTitle(" Railway Traffic Simulator ^_^ ");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 600);
        init();
        setVisible(true);
    }
    public void init(){
        this.setLayout(null);

        addTrain=new JButton("Add Train");
        addTrain.setBounds(10, 90, 90, 70);
        displaySeg=new JButton("Display Segment");
        displaySeg.setBounds(110, 90, 130, 70);
        train=new JTextField("Train");
        train.setBounds(10, 50, 90, 20);
        currentSeg=new JTextField("Segment");
        currentSeg.setBounds(110, 50, 130, 20);
        segmentdetails=new JTextArea();
        segmentdetails.setBounds(250, 50, 200, 300);
        isadded=new JTextArea();
        isadded.setBounds(250, 380, 170, 40);
        stationName= new JTextField("Station");
        stationName.setBounds(10,200,90,20);
        addstationAndsegments=new JButton("Add Station and Segments");
        addstationAndsegments.setBounds(10,250,190,30);
        add(addTrain);
        add(displaySeg);
        add(train);
        add(currentSeg);
        add(segmentdetails);
        add(isadded);
        add(addstationAndsegments);
        add(stationName);

        addstationAndsegments.addActionListener(new StationAndSegments());
        addTrain.addActionListener(new Train());
        displaySeg.addActionListener(new DisplaySeg());
    }
    class Station{
        String stn;
        Station(){

        }
        Station(String s){
            stn=s;
        }
        ArrayList<String> segments = new ArrayList<String>();
        void  addSeg (String s){
            segments.add(s);
        }
        Iterator<String> iter = segments.iterator();
        void seeseg(){
            while (iter.hasNext()) {
                if(currentSeg.getText().equals(iter.next())){
                    RailwayTrafficSimulator.this.segmentdetails.append(iter.next()+" "+stn);
                    if(RailwayTrafficSimulator.this.trains.containsKey(currentSeg.getText())){
                        RailwayTrafficSimulator.this.segmentdetails.append(" there is a train "+ trains.values());
                    }
                }

            }
        }
    }
    class StationAndSegments implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Station st=new Station(stationName.getText());
            st.addSeg(currentSeg.getText());
            isadded.append("Statie adaugata \n");
        }
    }
    class Train implements ActionListener{

        public void actionPerformed(ActionEvent e){
            isadded.append("");
            trains.put(train.getText(),currentSeg.getText());
            isadded.append("Tren Adaugat");
        }
    }

    class DisplaySeg implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {

        }
    }


    public static void main(String[] args){
        new RailwayTrafficSimulator();
    }

}
