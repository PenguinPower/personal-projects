package ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import javax.swing.*;

public class ButtonCount extends JFrame {
    JTextField user;
    JButton but;
    int k=0;
    ButtonCount(){
        setTitle("Button press counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);

        this.setLayout(null);

        user = new JTextField(" 0");
        user.setBounds(10, 50, 100, 100);

        but = new JButton("ClickMe");
        but.setBounds(10,150,100, 100);
        add(user);add(but);

        but.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                k++;
                user.setText(""+k);
            }

        });


        setVisible(true);
    }

    public static void main(String[] args) {
        ButtonCount c = new ButtonCount();


    }
}
