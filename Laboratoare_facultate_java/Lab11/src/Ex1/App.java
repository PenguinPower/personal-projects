package Ex1;
import javax.swing.*;
import java.awt.*;
import java.util.*;

class Sensor extends Observable implements Runnable{
    double temp=0;
    Thread thread;
    Random rnd=new Random();
    public void run()
    {
        while(true) {
            temp = rnd.nextInt(29)+1;
            this.setChanged();
            this.notifyObservers();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public double getTemp()
    {
        return temp;
    }
    public void start()
    {
        if(thread==null)
        {
            thread=new Thread(this);thread.start();
        }
    }

}
class TemperatureTextView extends JPanel implements Observer{
    JTextField jtfTemp;
    JLabel jtlTemp;
    TemperatureTextView()
    {
        this.setLayout(new FlowLayout());
        jtfTemp = new JTextField(20);
        jtlTemp = new JLabel("                    Temperature");
        add(jtlTemp);add(jtfTemp);
    }
    public void update(Observable o, Object arg) {
        String s = ""+((Sensor)o).getTemp()+" C";
        jtfTemp.setText(s);
    }

}

class TemperatureApp extends JFrame{
    TemperatureApp(TemperatureTextView tview)
    {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }
    public static void main(String[] args) {
        Sensor s = new Sensor();
        TemperatureTextView tview = new TemperatureTextView();
        s.addObserver(tview);
        s.start();

        new TemperatureApp(tview);
    }
}