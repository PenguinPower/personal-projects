package Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

class Product
{
    private String name,quantity,price;
    Product(String name, String quanity, String price)
    {
        this.name=name;
        this.quantity=quanity;
        this.price=price;
    }
    public String getName()
    {
        return this.name;
    }
    public String getQuanity()
    {
        return this.quantity;
    }
    public void setQuantity(String quantity)
    {
        this.quantity=quantity;
    }
    public String getPrice()
    {
        return this.price;
    }
    public String toString()
    {
        return this.name+"                               "+this.quantity+"                               "+this.price;
    }
}
class Products extends Observable
{
    ArrayList<Product> products= new ArrayList<>();
    void addProduct(Product product)
    {
        products.add(product);
        this.setChanged();
        this.notifyObservers();
    }
    public boolean deleteProduct(String Name)
    {   boolean deleted = false;
        for(Product i: products)
        {
            if(i.getName().equals(Name))
            {
                deleted=true;
                products.remove(i);
                break;
            }
        }
        this.setChanged();
        this.notifyObservers();
        return deleted;
    }

    public boolean ChangeQuanity(String quantity,String Name)
    {
        boolean changed=false;
        for(Product i: products)
        {
            if(i.getName().equals(Name))
            {
                changed=true;
                i.setQuantity(quantity);
                break;
            }
        }
        this.setChanged();
        this.notifyObservers();
        return changed;
    }
}
class ProductView extends JFrame implements Observer, ActionListener {

    private JLabel name,quantity,price,namedel,nameadd,quantityadd,priceadd,namech,quantitych;
    private JTextArea list;
    private JButton del,ad,ch;
    private JTextField delname,addname,addquantity,addprice,chname,chquantity;
    private Products prod=new Products();
    private Product p;
    ProductView()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(700,400);
        name=new JLabel("Name");
        name.setBounds(10,10,70,20);
        quantity=new JLabel("Quantity");
        quantity.setBounds(110,10,70,20);
        price=new JLabel("Price");
        price.setBounds(210,10,70,20);
        list=new JTextArea();
        list.setBounds(10,40,300,300);
        namedel=new JLabel("Name");
        namedel.setBounds(340,10,40,20);
        delname=new JTextField();
        delname.setBounds(340,40,70,20);
        del=new JButton("Delete");
        del.setBounds(420,40,100,20);
        nameadd=new JLabel("Name");
        nameadd.setBounds(340,80,40,20);
        addname=new JTextField();
        addname.setBounds(340,110,70,20);
        quantityadd= new JLabel("Quantity");
        quantityadd.setBounds(420,80,70,20);
        addquantity= new JTextField();
        addquantity.setBounds(420,110,70,20);
        priceadd=new JLabel("Price");
        addprice=new JTextField();
        priceadd.setBounds(500,80,70,20);
        addprice.setBounds(500,110,70,20);
        ad=new JButton("Add");
        ad.setBounds(580,110,100,20);
        namech=new JLabel("Name");
        namech.setBounds(340,140,40,20);
        chname=new JTextField();
        chname.setBounds(340,170,70,20);
        quantitych=new JLabel("Quantity");
        quantitych.setBounds(420,140,70,20);
        chquantity=new JTextField();
        chquantity.setBounds(420,170,70,20);
        ch=new JButton("Change");
        ch.setBounds(500,170,100,20);
        ad.addActionListener(this);
        del.addActionListener(this);
        ch.addActionListener(this);

        add(name);add(price);add(quantity);add(list);add(namedel);add(delname);add(del);add(nameadd);add(addname);
        add(quantityadd);add(addquantity);add(priceadd);add(addprice);add(ad);add(namech);add(chname);add(quantitych);
        add(chquantity);add(ch);
        setVisible(true);
        prod.addObserver(this);

    }
    @Override
    public void update(Observable o, Object arg) {
            list.setText(null);
            for (int i=0; i<((Products) o).products.size();i++) {
                list.append((((Products) o).products.get(i))+"\n");

        }


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button=(JButton) e.getSource();
        if(button.getText().equals("Add"))
        {
            p=new Product(addname.getText(),addquantity.getText(),addprice.getText());
            prod.addProduct(p);
            addname.setText("");addquantity.setText("");addprice.setText("");
        }
        if(button.getText().equals("Delete"))
        {   boolean deleted=prod.deleteProduct(delname.getText());
            if(!deleted)
                JOptionPane.showMessageDialog(null,"The product doesn't exists!");
            delname.setText("");
        }
        if(button.getText().equals("Change"))
        {
            boolean changed=prod.ChangeQuanity(chquantity.getText(),chname.getText());
            if(!changed)
                JOptionPane.showMessageDialog(null,"The product doesn't exists!");
            chquantity.setText(null);
            chname.setText(null);
        }
    }
}
class Main{
    public static void main(String[] args) {
        ProductView p =new ProductView();
    }
}
