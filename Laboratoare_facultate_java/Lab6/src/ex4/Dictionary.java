package ex4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class Dictionary {
    HashMap dictionary = new HashMap();

    public void addWord(Word c, Definition definitie) {

        if (dictionary.containsKey(c))
            System.out.println("Modific cuvant existent!");
        else
            System.out.println("Adauga cuvant nou.");
        dictionary.put(c, definitie);
    }

    public void getAllWords() {
        for (Object key : dictionary.keySet()) {
            System.out.println(key);
        }
    }

    public void getAllDefinitions() {
        for (Object key : dictionary.keySet()) {
            Object value = dictionary.get(key);
            System.out.println("DEFINITION FOR " + key + ": " + value);
        }
    }
    public static void main(String args[]) throws Exception {
        Dictionary dict = new Dictionary();
        char raspuns;
        String linie, explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Add");
            System.out.println("d - List definitions");
            System.out.println("w - List words");
            System.out.println("e - Exit");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch (raspuns) {
                case 'a':
                case 'A':
                    System.out.println("Word:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Definition:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'w':
                case 'W':
                    System.out.println("Listing words:");
                    dict.getAllWords();
                    break;
                case 'd':
                case 'D':
                    System.out.println("Listing definitions:");
                    dict.getAllDefinitions();
                    break;
            }
        }
        while (raspuns != 'e' && raspuns != 'E');
    }
}
