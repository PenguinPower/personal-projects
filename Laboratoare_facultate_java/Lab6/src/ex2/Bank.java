package ex2;
import ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Bank {
        static ArrayList l=new ArrayList();
        public void addBankAccount(String owner,double balance)
        {
            BankAccount b=new BankAccount(owner,balance);
            l.add(b);
        }
        public void printAccounts()
        {
            Collections.sort(l,new SortByBallance());
            for(int i=0;i<l.size();i++)
            {
                BankAccount b=(BankAccount)l.get(i);
                System.out.println(b.toString());
            }
        }
        public void printAccounts(double minBalance,double maxBalance)
        {
            Collections.sort(l,new SortByBallance());
            System.out.println("Accounts with balance between "+minBalance+" and "+maxBalance+" are: ");
            for(int i=0;i<l.size();i++)
            {
                BankAccount b=(BankAccount)l.get(i);
                if(b.getBalance()<maxBalance&&b.getBalance()>minBalance)
                    System.out.println(b.toString());
            }
        }
        public BankAccount getAccount(String owner)
        {
            BankAccount c=new BankAccount(null,0);
            for(int i=0;i<l.size();i++)
            {
                BankAccount b=(BankAccount)l.get(i);
                if(b.getOwner().equals(owner))
                    c=(BankAccount)l.get(i);
            }
            return c;
        }
        public static void getAllAccounts()
        {
            Collections.sort(l, new Comparator<BankAccount>() {
                @Override
                public int compare(final BankAccount object1, final BankAccount object2) {
                    return object1.getOwner().compareTo(object2.getOwner());
                }
            });
            for(int i=0;i<l.size();i++)
            {
                BankAccount b=(BankAccount)l.get(i);
                System.out.println(b.toString());
            }
        }

}
