package ex2;
import ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        Bank bank=new Bank();
        System.out.println("How many accounts do you want to create?");
        int n=s.nextInt();
        for(int i=0;i<n;i++)
        {
            System.out.println("Insert the owner and the balance!");
            String owner=s.next();
            double balance=s.nextDouble();
            bank.addBankAccount(owner,balance);
        }
        System.out.println("");
        bank.printAccounts();
        System.out.println("");
        bank.printAccounts(5,15);
        System.out.println("");
        System.out.println(bank.getAccount("Marian").toString());
        System.out.println("");
        bank.getAllAccounts();
    }
}
