package ex2;

import ex1.BankAccount;

import java.util.Comparator;

public class SortByBallance implements Comparator<BankAccount> {
    public int compare(BankAccount a,BankAccount b)
    {
        return (int)(a.getBalance()-b.getBalance());
    }
}
