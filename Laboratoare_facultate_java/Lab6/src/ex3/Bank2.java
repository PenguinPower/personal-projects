package ex3;
import ex2.SortByBallance;

import java.util.*;

public class Bank2 {
    static TreeSet l=new TreeSet();
    Iterator<BankAccount> iterator = l.iterator();
    public void addBankAccount(String owner,double balance)
    {
        BankAccount b=new BankAccount(owner,balance);
        l.add(b);
    }
    public void printAccounts()
    {
        iterator = l.iterator();
        while(iterator.hasNext())
        {
            BankAccount b=(BankAccount)iterator.next();
            System.out.println(b.toString());
        }
    }
    public void printAccounts(double minBalance,double maxBalance)
    {
        System.out.println("Accounts with balance between "+minBalance+" and "+maxBalance+" are: ");
        iterator = l.iterator();
        while(iterator.hasNext())
        {
            BankAccount b=(BankAccount)iterator.next();
            if(b.getBalance()<=maxBalance&&b.getBalance()>=minBalance)
                System.out.println(b.toString());
        }
    }
    public BankAccount getAccount(String owner)
    {
        BankAccount c=new BankAccount(null,0);
        iterator = l.iterator();
        while(iterator.hasNext())
        {
            BankAccount b=(BankAccount)iterator.next();
            if(b.getOwner().equals(owner))
                c=b;
        }
        return c;
    }
    public void getAllAccounts()
    {
        for (Object o : l) {
            System.out.println(o.toString());
        }
    }

}