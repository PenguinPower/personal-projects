package ex3;
public class BankAccount implements Comparable {
    private String owner;
    private double balance;
    public BankAccount(String owner,double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }
    public void withdraw(double amount)
    {
        this.balance-=amount;
    }
    public void deposit(double amount)
    {
        this.balance+=amount;
    }
    public boolean equals(Object obj)
    {
        if(obj instanceof BankAccount)
        {
            BankAccount b=(BankAccount)obj;
            return balance==b.balance;
        }
        else
            return false;
    }
    public int hashCode()
    {
        return owner.hashCode()+(int)balance;
    }
    public String getOwner()
    {
        return this.owner;
    }
    public double getBalance()
    {
        return this.balance;
    }
    public String toString()
    {
        return "Owner is "+this.owner+" and the balance is "+this.balance;
    }
    public int compareTo(Object a)
    {
        BankAccount b=(BankAccount)a;
        if(balance>b.balance)
            return 1;
        if(balance<b.balance)
            return -1;
        return 0;
    }
}
