package Ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer extends JFrame implements ActionListener{
    int seconds, minutes, hours;
    Timer chronometer;
    JButton StartStop, Restart;
    JLabel time;

    public Chronometer() {
        this.setSize(250,80);
        this.setLocationRelativeTo(null);
        this.setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));

        StartStop = new JButton("Run");
        StartStop.addActionListener(this);

        Restart = new JButton("Restart");
        Restart.addActionListener(this);

        seconds = 0;
        minutes = 0;
        hours = 0;

        time=new JLabel(hours+ " : "+minutes+" : "+seconds);
        this.add(StartStop);
        this.add(Restart);
        this.add(time);
        chronometer = new Timer(1000, this);
    }

    public static void main(String[] args) {
        Chronometer chronometer=new Chronometer();
        chronometer.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof Timer) {
            seconds++;
            if (seconds == 60) {
                minutes++;
                seconds = 0;
                if (minutes == 60) {
                    hours++;
                    minutes = 0;
                }
            }
            time.setText(hours+ " : "+minutes+" : "+seconds);
            return;
        }else if(e.getSource() instanceof JButton){
            JButton button = (JButton) e.getSource();
            if(button.getText().equals("Run")){
                chronometer.start();
                button.setText("Stop");
            }else if(button.getText().equals("Stop")){
                chronometer.stop();
                button.setText("Run");
            }else if(button.getText().equals("Restart")){
                seconds=0;
                minutes=0;
                hours=0;
                chronometer.stop();
                time.setText(hours+ " : "+minutes+" : "+seconds);
                StartStop.setText("Start");
            }

        }

    }

}
