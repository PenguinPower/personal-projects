package Ex1;

class CounterRun extends Thread {

    CounterRun(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
        CounterRun c1 = new CounterRun("counter1");
        CounterRun c2 = new CounterRun("counter2");
        CounterRun c3 = new CounterRun("counter3");

        c1.run();
        c2.run();
        c3.run();
    }
}