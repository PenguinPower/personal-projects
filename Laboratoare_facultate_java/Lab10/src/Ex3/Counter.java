package Ex3;

class Counter1 extends Thread {
    Counter1 (String name)
    {
        super(name);
    }
    public void run()
    {
        for(int i=1;i<=100;i++)
        {
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}
class Counter2 extends Thread {
    Counter2 (String name)
    {
        super(name);
    }
    public void run()
    {
        for(int i=101;i<=200;i++)
        {
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}
class Main {

    public static void main(String[] args) {
        Counter1 c1=new Counter1("counter1");
        Counter2 c2=new Counter2("counter2");
        c1.run();
        c2.run();
    }
}

