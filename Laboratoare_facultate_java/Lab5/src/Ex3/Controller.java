package Ex3;
import java.util.concurrent.TimeUnit;
import java.lang.*;
public class Controller {
    public static void control()
    {   TemperatureSensor ts=new TemperatureSensor();
        LigthSensor ls=new LigthSensor();
        System.out.println("Senzorul de lumina din "+ls.getLocation()+" are valoarea: "+ls.readValue());
        System.out.println("Senzorul de temperatura din "+ts.getLocation()+" are valoarea: "+ts.readValue());
    }
    public static void main(String[] args)
    {
        for(int i=0;i<20;i++)
        {
            control();
            try {
                Thread.sleep( 1000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
