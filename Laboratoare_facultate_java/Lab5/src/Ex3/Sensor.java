package Ex3;

public abstract class Sensor {
    private String location="Bedroom";
    public Sensor()
    {}
    public String getLocation()
    {
        return this.location;
    }
    public abstract int readValue();
}
