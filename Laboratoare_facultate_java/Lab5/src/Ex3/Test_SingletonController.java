package Ex3;

public class Test_SingletonController {
    public static void main(String[] args)
    {
        ControllerSingleton c=ControllerSingleton.getInstance();
        for(int i=0;i<20;i++)
        {
            c.control();
            try {
                Thread.sleep( 1000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
