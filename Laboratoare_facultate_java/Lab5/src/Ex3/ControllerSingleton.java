package Ex3;
import java.util.concurrent.TimeUnit;
import java.lang.*;
public class ControllerSingleton {
    public static ControllerSingleton single_controller=null;
    public void control()
    {   TemperatureSensor ts=new TemperatureSensor();
        LigthSensor ls=new LigthSensor();
        System.out.println("Senzorul de lumina din "+ls.getLocation()+" are valoarea: "+ls.readValue());
        System.out.println("Senzorul de temperatura din "+ts.getLocation()+" are valoarea: "+ts.readValue());
    }
    public static ControllerSingleton getInstance()
    {
        synchronized(ControllerSingleton.class)
        {
            if(single_controller==null)
                single_controller=new ControllerSingleton();
        }
        return single_controller;

    }
}
