package Ex1;

public class Rectangle extends Shape {
    private double width,length;
    public Rectangle()
    {

    }
    public Rectangle(double width,double length)
    {
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width,double length,String color,boolean filled)
    {
        this.width=width;
        this.length=length;
        this.color=color;
        this.filled=filled;
    }
    public double getWidth()
    {
        return this.width;
    }
    public double getLength()
    {
        return this.length;
    }
    public void setWidth(double width)
    {
        this.width=width;
    }
    public void setLength(double length)
    {
        this.length=length;
    }
    public double getArea()
    {
        return this.width*this.length;
    }
    public double getPerimeter()
    {
        return this.length*2+this.width*2;
    }
    public String toString()
    {
        if(this.filled)
            return "Dreptunghi cu laturile de: "+this.width+","+this.length+", avand culaorea "+this.color+", acesta fiind umplut";
        else
            return "Dreptunghi cu laturile de: "+this.width+","+this.length+", avand culaorea "+this.color+", acesta ne fiind umplut";
    }
}
