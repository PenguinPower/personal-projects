package Ex1;

public class Square extends Rectangle {
    public Square()
    {

    }
    public Square(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public Square(double side,String color,boolean filled)
    {
        this.setLength(side);
        this.setWidth(side);
        this.color=color;
        this.filled=filled;
    }
    public double getSide()
    {
        return this.getLength();
    }
    public void setSide(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public void setLength(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public void setWidth(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public String toString()
    {
        if(this.filled)
            return "Patrat de latura "+this.getLength()+", avand culaorea "+this.color+", acesta fiind umplut";
        else
            return "Patrat de latura "+this.getLength()+", avand culaorea "+this.color+", acesta ne fiind umplut";
    }
}
