package Ex1;
import java.lang.Math;
public class Circle extends Shape {
    private double radius;
    public Circle()
    {

    }
    public Circle(double radius)
    {
        this.radius=radius;
    }
    public Circle(double radius,String color,boolean filled)
    {
        this.radius=radius;
        this.filled=filled;
        this.color=color;
    }
    public double getRadius()
    {
        return this.radius;
    }
    public void setRadius(double radius)
    {
        this.radius=radius;
    }
    public double getArea()
    {
        return Math.PI*Math.pow(this.radius,2);
    }
    public double getPerimeter()
    {
        return 2*Math.PI*this.radius;
    }
    public String toString()
    {
        if(this.filled)
            return "Cercul de raza "+this.radius+", avand culoarea "+this.color+" acesta fiind umplut.";
        else
            return "Cercul de raza "+this.radius+", avand culoarea "+this.color+" acesta ne fiind umplut.";
    }
}
