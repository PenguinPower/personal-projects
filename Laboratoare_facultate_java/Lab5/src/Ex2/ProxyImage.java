package Ex2;

public class ProxyImage implements Image {

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private boolean real = false;
    private String fileName;

    public ProxyImage(String fileName, boolean real) {
        this.fileName = fileName;
        this.real = real;
    }

    @Override
    public void display() {
        if (real) {
            if (realImage == null) {
                realImage = new RealImage(this.fileName);
            }
            realImage.display();
        } else {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(this.fileName);
            }
            rotatedImage.display();
        }
    }

    public static void main(String[] args) {
        ProxyImage i1 = new ProxyImage("imagine reala.jpg", true), i2 = new ProxyImage("imagine rotita.jpg", false);
        i1.display();
        i2.display();
    }
}