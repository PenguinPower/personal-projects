import java.util.Random;
public class GenArray {
    private static void bubblesort(int[] x)
    {
        int b = x.length;
        for (int i = 0; i < b-1; i++)
            for (int j = 0; j < b-i-1; j++)
                if (x[j] > x[j+1])
                {
                    // swap arr[j+1] and arr[i]
                    int temp = x[j];
                    x[j] = x[j+1];
                    x[j+1] = temp;
                }

    }
    public static void main(String[] args)
    {
        Random rand=new Random();
        int[] v=new int[10];
        for(int i=0;i<10;i++)
            v[i]=rand.nextInt(1000);
        System.out.print("Vectorul initial: ");
        for(int i=0;i<10;i++)
        {
            System.out.print(" " + v[i] + " ");
        }
        System.out.println(" ");
        bubblesort(v);
        System.out.print("Vectorul sortat: ");
        for(int i=0;i<10;i++)
        {
         System.out.print(" " + v[i] + " ");
        }

    }
}
