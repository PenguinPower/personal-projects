import java.util.Scanner;
import java.util.Random;
public class GuessTheNumber {

    public static void main(String[] args)
    {
        Random rand=new Random();
        Scanner s=new Scanner(System.in);
        int life=3,x,y;
        System.out.println("Ghiceste numarul intre 0 si 9");
        x=rand.nextInt(10);
        while(life>0)
        {
            y=s.nextInt();
            if(y==x)
            {
                break;
            }
            if(y<x)
            {
                System.out.println("Wrong answer, your number is too low!");
                life--;
            }
            if(y>x)
            {
                System.out.println("Wrong answer, your number is too high!");
                life--;
            }
        }
        if(life==0) {
            System.out.println("You lost!");
            System.out.println("The number was " + x);
        }
        else {
            System.out.println("You WON!");
            System.out.println("The number is indeed " + x);
        }

    }
}
