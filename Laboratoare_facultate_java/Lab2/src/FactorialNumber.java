import java.util.Scanner;
public class FactorialNumber {
    private static double factorial(int x)
    {   if(x==0)
            return 1;
        else
            return x*factorial(x-1);
    }
    public static void main(String[] args)
    {
        Scanner s=new Scanner(System.in);
        System.out.println("Dati numarul n: ");
        int n=s.nextInt();
        System.out.println("Factorialul numarului " + n + " este  " + factorial(n));

    }
}
