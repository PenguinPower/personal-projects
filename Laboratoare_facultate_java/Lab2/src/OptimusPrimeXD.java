import java.util.Scanner;

public class OptimusPrimeXD {

   private static boolean prim(int g)
    {
        int i;
        boolean ok=true;
        for(i=2;i<=g/2;i++)
        {
            if(g%i==0)
            {
                ok = false;
                break;
            }
        }
        return ok;
    }
    public static void main(String[] args)
    {
      Scanner s=new Scanner(System.in);
      int a,b,i,n=0;
      System.out.println("Dati intervalul: ");
      a=s.nextInt();
      b=s.nextInt();
      i=a;
      while(i<b)
      {
        if(prim(i))
        {
            System.out.println("S-a gasit un numar prim: "+i);
            n++;
        }
        i++;

      }
      System.out.println("In total au fost " + n + " numere prime");
    }
}
