import java.util.Scanner;
public class PrintNumberInWord {
    public static void main (String[] args)
    {
        Scanner s=new Scanner(System.in);
        int a;
        System.out.println("Dati un numar de la 0 la 9: ");
        a=s.nextInt();
        while(a<0||a>9)
        {
            System.out.println("Dati un numar de la 0 la 9: ");
            a=s.nextInt();
        }
        switch(a)
        {
            case 0: {System.out.println("Zero!");break;}
            case 1: {System.out.println("Unu!");break;}
            case 2: {System.out.println("Doi!");break;}
            case 3: {System.out.println("Trei!");break;}
            case 4: {System.out.println("Patru!");break;}
            case 5: {System.out.println("Cinci!");break;}
            case 6: {System.out.println("Sase!");break;}
            case 7: {System.out.println("Sapte!");break;}
            case 8: {System.out.println("Opt!");break;}
            case 9: {System.out.println("Noua!");break;}
            default: System.out.println("Error");
        }

    }
}
