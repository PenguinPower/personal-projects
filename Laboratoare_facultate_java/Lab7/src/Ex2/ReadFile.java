package Ex2;
import java.io.*;
import java.util.Scanner;
public class ReadFile {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.println("What character do you want to count?");
        Character c=s.next().charAt(0);
        int count=0;
        try {
            File myObj = new File("C:\\Users\\isp\\tataran-vasile-stefan-gr30123-isp-2020\\Lab7\\src\\Ex2\\data.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                count+=data.chars().filter(ch->ch==c).count();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("End of data.");
            e.printStackTrace();
        }
        System.out.println("In the file we can find "+c+": "+count+" times");
    }
}
