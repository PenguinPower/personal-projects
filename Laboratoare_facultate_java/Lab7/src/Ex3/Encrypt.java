package Ex3;
import java.io.*;
import java.util.*;
public class Encrypt {
    public String encrypt(String data)
    {
        String encrypted="";
        for (int i=0;i<data.length();i++)
        {
            int ascii=data.charAt(i);
            Character c=(char)(ascii-1);
            encrypted+=c;
        }
        return encrypted;
    }
}
