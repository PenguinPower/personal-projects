package Ex3;
import java.util.Scanner;
import java.io.*;
public class FileWrite {
    public void writing(String filename,String data) throws IOException {
            /*FileWriter myWriter = new FileWriter(filename);
            myWriter.write(data);
            myWriter.write("\n");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }*/
        File fout = new File(filename);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(data);
        bw.newLine();
        bw.close();
    }
}
