package Ex1;

public class TextCoffee {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            }catch (CofeeMakerException e){
                System.out.println("Exception "+e.getMessage());
            }
            catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}
