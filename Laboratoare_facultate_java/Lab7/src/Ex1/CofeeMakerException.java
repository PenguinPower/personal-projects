package Ex1;

public class CofeeMakerException extends Exception {
    int count;
    public CofeeMakerException(int count, String msg)
    {
        super(msg);
        this.count=count;
    }
    int getCount()
    {
        return this.count;
    }
}
