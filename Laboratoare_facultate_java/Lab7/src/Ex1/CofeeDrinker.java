package Ex1;

public class CofeeDrinker {
    void drinkCofee(Cofee c) throws CofeeMakerException,TemperatureException, ConcentrationException{
        if(c.getNumb()>10)
            throw new CofeeMakerException(c.getNumb(),"Too tired to make another coffee,come back tomorrrow!");
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Ex1.Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Ex1.Cofee concentration to high!");

        System.out.println("Drink cofee:"+c);
    }
}
