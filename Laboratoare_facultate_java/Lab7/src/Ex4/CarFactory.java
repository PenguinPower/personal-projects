package Ex4;
import java.util.*;
import java.io.*;
public class CarFactory {
    Car createCar(String model,double price)
    {
        Car c=new Car(model,price);
        System.out.println("A car was created!");
        return c;
    }
    void saveCar(Car c,String filename)throws IOException
    {
        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream(filename));

        o.writeObject(c);
        System.out.println(c+": car saved");
    }
    Car loadCar(String filename)throws IOException, ClassNotFoundException
    {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(filename));
        Car x = (Car)in.readObject();
        System.out.println(x);
        return x;
    }

}
