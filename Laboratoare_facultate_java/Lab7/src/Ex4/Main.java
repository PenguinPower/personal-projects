package Ex4;
import java.util.Scanner;
public class Main {
    public static void main(String[] args)throws Exception {
        CarFactory cf=new CarFactory();
        Scanner s=new Scanner(System.in);
        System.out.println("Choose a-to add a car (it will be saved in a folder with the name model.txt, where model in cars model");
        System.out.println("l- to load a car and see the price");
        System.out.println("e-to close the program");
        Character option=s.next().charAt(0);
        while(option!='e'&&option!='E')
        {
            switch(option) {
                case 'a':
                case 'A': {
                    System.out.println("Give the cars model and the price!");
                    String model = s.next();
                    double price = s.nextDouble();
                    Car c = cf.createCar(model, price);
                    cf.saveCar(c, c.model);
                    break;
                }
                case 'l':
                case 'L': {
                    System.out.println("Looking for model: ");
                    String model = s.next();
                    Car c = cf.loadCar(model);
                    break;
                }
            }
            System.out.println("Choose a-to add a car (it will be saved in a folder with the name model.txt, where model in cars model");
            System.out.println("l- to load a car and see the price");
            System.out.println("e-to close the program");
            option=s.next().charAt(0);
        }
        System.out.println("Program over!");
    }
}
