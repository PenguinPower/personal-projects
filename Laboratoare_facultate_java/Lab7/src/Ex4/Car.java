package Ex4;

import java.io.Serializable;

public class Car implements Serializable {
    String model;
    double price;
    public Car(String name,double price)
    {
        this.model=name;
        this.price=price;
    }
    public String toString()
    {
        return "Model "+this.model+" has a price of "+this.price+" euro.";
    }
}
