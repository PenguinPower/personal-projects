import java.util.Scanner;
public class TestCircle {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Circle c1 = new Circle();
        System.out.println("Raza cercului c1 este " + c1.getRadius() + " iar aria sa este " + c1.getArea());
        double rad;
        System.out.println("Dati culoarea cercului:");
        String color = s.nextLine();
        ;
        System.out.println("Dati raza cercului: ");
        rad = s.nextDouble();
        Circle c2 = new Circle(rad, color);
        System.out.println("Raza cercului c2 este " + c2.getRadius() + " iar aria sa este " + c2.getArea());
    }
}
