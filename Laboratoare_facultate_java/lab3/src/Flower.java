public class Flower{
    int petal;
    private static int k=0;
    Flower(){
        k++;
        System.out.println("Flower has been created! The total number of flowers is: "+k);
    }
    public int getK()
    {
        return k;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("Numarul total de flori este: "+garden[2].getK());
    }

}