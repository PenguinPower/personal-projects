import java.lang.Math;
public class MyPoint {
    private int x,y;
    MyPoint()
    {
        this.x=0;
        this.y=0;
        System.out.println("MyPoint a fost creat la coordonatele ("+x+","+y+")");
    }
    MyPoint(int x,int y)
    {
        this.x=x;
        this.y=y;
        System.out.println("MyPoint a fost creat la coordonatele ("+x+","+y+")");
    }
    public void setX(int x)
    {
        this.x=x;
    }
    public void setY(int y)
    {
        this.y=y;
    }
    public int getX()
    {
        //System.out.println("Valoarea lui x este: ");
        return this.x;
    }
    public int getY()
    {
        //System.out.println("Valoarea lui y este: ");
        return this.y;
    }
    public void setXY(int x, int y)
    {
        this.x=x;
        this.y=y;
    }
    public String toString()
    {
        return "("+x+","+y+")";
    }
    public void distance(int x, int y)
    {
        double xx=Math.pow(this.x-x,2);
        double yy=Math.pow(this.y-y,2);
        double d=xx+yy;
        System.out.println("Distante dintre punctele " + this.toString()+" si ("+x+","+y+") este: " + Math.sqrt(d));
    }
    public void distance(MyPoint d)
    {
        double xx=Math.pow(this.x-d.getX(),2);
        double yy=Math.pow(this.y-d.getY(),2);
        double g=xx+yy;
        System.out.println("Distanta dintre punctele "+this.toString()+" si "+d.toString()+" este: "+Math.sqrt(g));
    }
}
