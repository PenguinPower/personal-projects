public class Robot {
    private int x;
    Robot()
    {
        this.x=1;
        System.out.println("Pozitia robotului a fost setata la 1!");
    }
    public void change(int k)
    {
        while(k>=1)
        {
            this.x++;
            k--;
            System.out.println("Robotul a avansat o pozitie. Pozitia curenta este: " + toString());
        }
    }
    public String toString()
    {
        return " "+this.x+" ";
    }

}
