public class Author {
    private String name,email;
    private Character gender;
    Author(String name,String email,Character gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;
        System.out.println("Autorul: " + toString());
    }
    public void setEmail(String email)
    {
        this.email=email;
        System.out.println("Adresa de email a fost schimbata in " + this.email);
    }
    public String getEmail()
    {
        return this.email;
    }
    public String getName()
    {
        return this.name;
    }
    public Character getGender()
    {
        return this.gender;
    }
    public String toString()
    {
        return '"'+this.getName()+" ("+this.getGender()+") "+"at "+this.getEmail()+'"';
    }
}
