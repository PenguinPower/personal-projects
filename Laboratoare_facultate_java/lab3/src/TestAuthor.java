import java.util.Scanner;
public class TestAuthor {
    public static void main(String[] args)
    {
        Scanner s=new Scanner(System.in);
        System.out.println("Introduceti numele, apoi adresa de email: ");
        String name=s.nextLine(),email=s.nextLine();
        System.out.println("Introduceti genul autoruli (m) pentru masculin, iar (f) pentru femini: ");
        Character gender=s.next().charAt(0);
        while(gender!='m'&&gender!='f')
        {
            System.out.println("Gen incorect. Introduceti (m) pentru masculin sau (f) pentru feminin!");
            gender=s.next().charAt(0);
        }
        Author a=new Author(name,email,gender);
        System.out.println(a.toString());
        System.out.println("Schimbati adresa de email: ");
        System.out.flush();
        String email2=s.next();
        a.setEmail(email2);
    }
}
