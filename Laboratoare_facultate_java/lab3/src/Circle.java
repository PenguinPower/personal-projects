import java.lang.Math.*;
public class Circle {
    private double radius;
    private String color;
    Circle()
    {
        this.radius=1;
        this.color="red";
        System.out.println("Cercul a fost initializat cu raza de " + this.radius + " si culoarea " + this.color);
    }
    Circle(double rad,String color)
    {
        this.radius=rad;
        this.color=color;
        System.out.println("Cercul a fost initializat cu raza de " + this.radius + " si culoarea " + this.color);
    }
    public double getRadius()
    {
        return this.radius;
    }
    public double getArea()
    {   double area=Math.PI*(this.radius*this.radius);
        return area;
    }
}
