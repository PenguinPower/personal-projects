package ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeAutomation {
    public static void main(String[] args)throws IOException {

        //test using an annonimous inner class
        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
            }
            protected void controllStep(){
                System.out.println("Control step executed");
            }
        };
        int temp;
        boolean sm;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("control steps:");
        temp = Integer.parseInt(br.readLine());
        sm=Boolean.parseBoolean(br.readLine());
        h.simulate(temp,sm);
    }
}
