package ex4;

import java.util.Random;

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    private int currentTemp;

    protected abstract void setValueInEnvironment(Event event);
    protected abstract void controllStep();

    Home(){
    }

    private Event getHomeEvent(){
        //randomly generate a new event;
        int k = r.nextInt(100);
        if(k<30)
            return new NoEvent();
        else if(k<60){

            return new FireEvent(r.nextBoolean());
        }
        else
            return new TemperatureEvent(r.nextInt(50));

    }
    protected void VerifyTemperature (TemperatureEvent te){
        if(te.getVlaue()< currentTemp)
            te.setValue(currentTemp);
        if(te.getVlaue()>currentTemp)
            te.setValue(currentTemp);
    }

    public void simulate(int ste,boolean sfe){
        int k = 0;
        while(k <SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();
            TemperatureEvent te= new TemperatureEvent(ste);
            VerifyTemperature(te);
            FireEvent fe=new FireEvent(sfe);
            fe.VerifyAlarm();
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }
}