package ex4;

public class FireSensor {
    public boolean checkSmoke(FireEvent fireEvent){
        return fireEvent.isSmoke();
    }
}
