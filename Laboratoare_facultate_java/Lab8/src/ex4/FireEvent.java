package ex4;

class FireEvent extends Event {

        private boolean smoke;

        FireEvent(boolean smoke) {
            super(EventType.FIRE);
            this.smoke = smoke;
        }

        boolean isSmoke() {
            return smoke;
        }
        void VerifyAlarm(){
            if(smoke==true)
                System.out.println("Alarm! Call de owner!");
            else
                System.out.println("Safe.");
        }
        @Override
        public String toString() {
            return "FireEvent{" + "smoke=" + smoke + '}';
        }

    }