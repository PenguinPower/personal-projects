package Ex5;

public class TestCylinder {

    public static void main(String[] args)
    {
        Cylinder c1=new Cylinder(),c2=new Cylinder(2),c3=new Cylinder(3,3);
        System.out.println("Inaltimile cilindrelor sunt: ");
        System.out.println("c1: "+c1.getHeight() + " raza de "+c1.getRadius());
        System.out.println("c2: "+c2.getHeight() + " raza de "+c2.getRadius());
        System.out.println("c3: "+c3.getHeight() + " raza de "+c3.getRadius());
        System.out.println("Volumele sunt: ");
        System.out.println("c1: "+c1.getVolume());
        System.out.println("c2: "+c2.getVolume());
        System.out.println("c3: "+c3.getVolume());
    }
}
