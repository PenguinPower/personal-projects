package Ex5;
import Ex1.Circle;
import java.lang.Math;
public class Cylinder extends Circle{
    private double height=1;
    public Cylinder()
    {

    }
    public Cylinder(double radius)
    {
        this.radius=radius;
    }
    public Cylinder(double radius,double height)
    {
        this.radius=radius;
        this.height=height;
    }
    public double getHeight()
    {
        return this.height;
    }
    public double getVolume()
    {
        return Math.PI*this.getRadius()*this.getRadius()*this.height;
    }
}
