package Ex1;

import java.lang.Math;
public class Circle {
    protected double radius=1;
    private String color="red";
    public Circle()
    {
        System.out.println("A fost creat cercul de raza: "+this.radius+" si culaorea: "+this.color);
    }
    public Circle(double rad)
    {
        this.radius=rad;
        System.out.println("A fost creat cercul de raza: "+this.radius+" si culaorea: "+this.color);
    }
    public double getRadius()
    {
        return this.radius;
    }
    public double getArea()
    {
        return Math.PI*radius*radius;
    }
}
