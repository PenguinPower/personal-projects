package Ex1;

import Ex1.Circle;

import java.util.Scanner;
public class TestCircle {
    public static void main(String[] args)
    {
        Scanner s= new Scanner(System.in);
        Circle c1=new Circle();
        System.out.println("Dati valoarea razei cercului: ");
        double radius=s.nextDouble();
        Circle c2=new Circle(radius);
        System.out.println("Raza cercului c1 este "+c1.getRadius()+", iar aria sa este: "+c1.getArea());
        System.out.println("Raza cercului c2 este "+c2.getRadius()+", iar aria sa este: "+c2.getArea());
    }
}
