package Ex4;
import Ex2.Author;
import java.util.Scanner;
public class TestBook {
    public static void main(String[] args)
    {
        Scanner s=new Scanner(System.in);
        System.out.println("Cartea se numeste: ");
        String name=s.next();
        System.out.println("Cati autori are cartea? Cartea nu poate avea mai mult de 10 autori!");
        int n=s.nextInt(),i;
        if(n>10)
        {
            System.out.println("Numar maxim de autori depasit! Se iau in considerare doar primii 10 autori.");
            n=10;
        }
        Author[] authors=new Author[n];
        for(i=0;i<n;i++)
        {
            System.out.println("Numele autorului numarul "+i+" este: ");
            String author_name=s.next();
            System.out.println("Adresa de email este: ");
            String email=s.next();
            System.out.println("Introduceti m pentru masculin sau f pentru feminin!");
            Character gender=s.next().charAt(0);
            while(gender!='m'&&gender!='f')
            {
                System.out.println("Gen incorect! Introduceti m pentru masculin sau f pentru femini!");
                gender=s.next().charAt(0);
            }
            authors[i]=new Author(author_name,email,gender);
        }
        /*System.out.println(authors.length+"  /  "+ authors[authors.length-1]);
        for(i=0;i<authors.length;i++) {
            System.out.println(authors[i].toString());
        }*/
        System.out.println("Introduceti pretul cartii");
        double price=s.nextDouble();
        Book book1=new Book(name,authors,price);
        for(i=0;i<book1.getAuthors().length;i++)
        {
            System.out.println(book1.getAuthors()[i].toString());
        }
        System.out.println("Se va testa printAuthors()");
        book1.printAuthors();
    }
}
