package Ex4;
import Ex2.Author;
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStore=0;
    public Book(String name,Author[] authors,double price)
    {
        this.authors=new Author[authors.length];
        this.name=name;
        this.price=price;
        for (int i=0;i<authors.length;i++)
        {
            this.authors[i]=authors[i];
        }
    }
    public Book(String name,Author[] authors,double price,int qtyInStore)
    {
        this.name=name;
        this.price=price;
        for (int i=1;i<=authors.length;i++)
        {
            this.authors[i]=authors[i];
        }
        this.qtyInStore=qtyInStore;
    }
    public int getQtyInStore()
    {
        return this.qtyInStore;
    }
    public void setPrice(double price)
    {
        this.price=price;
        System.out.println("Pretul a fost schimbat la "+this.price+" ron.");
    }
    public void setQtyInStore(int qtyInStore)
    {
        this.qtyInStore=qtyInStore;
        System.out.println("Numarul de carti in stoc este acum "+this.qtyInStore);
    }
    public double getPrice()
    {
        return this.price;
    }
    public String getName()
    {
        return this.name;
    }
    public Author[] getAuthors()
    {
        return this.authors;
    }
    public String toString()
    {
        String x=this.name+" by "+this.authors[1].toString();
        for(int i=2;i<=this.authors.length;i++)
        {
            x+=" and "+this.authors[i].toString();
        }
        return x;
    }
    public void printAuthors()
    {
        System.out.println("Autorii sunt: ");
        for(int i=0;i<this.authors.length;i++)
        {
            System.out.println(this.authors[i].toString());
        }
    }
}
