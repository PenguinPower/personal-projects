package Ex3;
import Ex2.Author;
public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStore;
    public Book(String name,Author author,double price)
    {
        this.name=name;
        this.author=author;
        this.price=price;
        System.out.println("A fost creata cartea cu numele "+this.name+",autorul "+author.toString()+", avand pretul de "+this.price+" ron");

    }
    public Book(String name,Author author,double price,int qtyInStore)
    {
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStore=qtyInStore;
        System.out.println("A fost creata cartea cu numele "+this.name+",autorul "+author.toString()+", avand pretul de "+this.price+" ron, iar numarul de carti in stoc este "+this.qtyInStore);

    }
    public String getName()
    {
        return this.name;
    }
    public Author getAuthor()
    {
        return this.author;
    }
    public double getPrice()
    {
        return this.price;
    }
    public int getQtyInStore()
    {
        return this.qtyInStore;
    }
    public void setPrice(double price)
    {
        this.price=price;
        System.out.println("Pretul a fost schimbat la "+this.price+" ron.");
    }
    public void setQtyInStore(int qtyInStore)
    {
        this.qtyInStore=qtyInStore;
        System.out.println("Numarul de carti in stoc este acum "+this.qtyInStore);
    }
    public String toString()
    {
        return this.getName()+ " by "+author.toString();
    }
}
