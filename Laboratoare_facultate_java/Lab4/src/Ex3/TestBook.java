package Ex3;
import java.util.Scanner;
import Ex2.Author;
public class TestBook {
    public static void main(String[] args)
    {
        Scanner s=new Scanner(System.in);
        System.out.println("Introducei numele cartii, numele autorului si adresa sa de email!");
        String name=s.next(),author_name=s.next(),email=s.next();
        System.out.println("Introduceti m pentru masculin sau f pentru femini");
        Character gender=s.next().charAt(0);
        while(gender!='m'&&gender!='f')
        {
            System.out.println("Gen incorect! Introduceti m pentru masculin sau f pentru femini!");
            gender=s.next().charAt(0);
        }
        Author author=new Author(author_name,email,gender);
        System.out.println("Introduceti pretul cartii: ");
        double price=s.nextDouble();
        Book book=new Book(name,author,price);
        System.out.println(book.toString());
        System.out.println("Introducei numele cartii, numele autorului si adresa sa de email!");
        String name2=s.next(),author_name2=s.next(),email2=s.next();
        System.out.println("Introduceti m pentru masculin sau f pentru femini");
        Character gender2=s.next().charAt(0);
        while(gender2!='m'&&gender2!='f')
        {
            System.out.println("Gen incorect! Introduceti m pentru masculin sau f pentru femini!");
            gender2=s.next().charAt(0);
        }
        Author author2=new Author(author_name2,email2,gender2);
        System.out.println("Introduceti pretul cartii: ");
        double price2=s.nextDouble();
        System.out.println("Introduceti numarul de exemplare aflate in stoc: ");
        int qtyInStore=s.nextInt();
        Book book2=new Book(name2,author2,price2,qtyInStore);
        System.out.println(book2.toString());
        System.out.println("Schimbati pretul primei carti, introduceti cantitatea sa in stoc: ");
        double k=s.nextDouble();
        book.setPrice(k);
        int t=s.nextInt();
        book.setQtyInStore(t);
        System.out.println("Cartea "+book.getName()+" cu autorul "+book.getAuthor().toString()+" la pretul de "+book.getPrice()+" ron, se gaseste intr-un numar total de exemplare de "+book.getQtyInStore());
        System.out.println("Cartea "+book2.getName()+" cu autorul "+book2.getAuthor().toString()+" la pretul de "+book2.getPrice()+" ron, se gaseste intr-un numar total de exemplare de "+book2.getQtyInStore());
    }
}
