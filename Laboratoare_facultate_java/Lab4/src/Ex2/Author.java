package Ex2;

public class Author {
    private String name,email;
    private Character gender;
    public Author(String name,String email,Character gender)
    {
        this.name=name;
        this.email=email;
        this.gender=gender;
        System.out.println("Autorul se numeste "+this.name+",adresa de email este "+this.email+",iar acesta este "+this.gender);
    }
    public String getName()
    {
        return this.name;
    }
    public String getEmail()
    {
        return this.email;
    }
    public Character getGender()
    {
        return this.gender;
    }
    public void setEmail(String email)
    {
        this.email=email;
        System.out.println("Adresa de email a fost schimbata in "+this.email);
    }
    public String toString()
    {
        return '"'+this.getName()+" ("+this.getGender()+") at "+this.getEmail()+'"';
    }

}
