package Ex6;
import java.lang.Math;
public class Rectangle extends Shape{
    private double width=1,length=1;
    public Rectangle(){}
    public Rectangle(double width,double length)
    {
        this.length=length;
        this.width=width;
    }
    public Rectangle(double width,double length,String color,Boolean filled)
    {
        this.width=width;
        this.length=length;
        this.setColor(color);
        this.setFilled(filled);
    }
    public double getWidth()
    {
        return this.width;
    }
    public double getLength()
    {
        return this.length;
    }
    public void setWidth(double width)
    {
        this.width=width;
        System.out.println("Width a fost schimbat in "+this.width);
    }
    public void setLength(double length)
    {
        this.length=length;
        System.out.println("Length a fost schimbat in "+this.length);
    }
    public double getArea()
    {
        return this.length*this.width;
    }
    public double getPerimeter()
    {
        return 2*this.width+2*this.length;
    }
    public String toString()
    {
        return "A Rectangle with width="+this.width+" and length="+this.length+", which is a subclass of "+super.toString();
    }
}
