package Ex6;
import java.lang.Math;
public class Circle extends Shape {
    private double radius=1;
    public Circle()
    {

    }
    public Circle(double radius)
    {
        this.radius=radius;
    }
    public Circle(double radius,String color,Boolean filled)
    {
        this.radius=radius;
        this.setColor(color);
        this.setFilled(filled);
    }
    public double getRadius()
    {
        return this.radius;
    }
    public void setRadius(double radius)
    {
        this.radius=radius;
    }
    public double getArea()
    {
        return Math.PI*Math.pow(this.radius,2);
    }
    public double getPerimeter()
    {
        return Math.PI*this.radius*2;
    }
    public String toString()
    {
        return "A Circle with radius = "+this.radius+", which is a subclass of "+super.toString();
    }
}
