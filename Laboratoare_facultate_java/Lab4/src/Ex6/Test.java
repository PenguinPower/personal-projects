package Ex6;
import java.util.Scanner;
public class Test {
    private static void testCircle()
    {
        Circle c1=new Circle(),c2=new Circle(5),c3=new Circle(3,"green",true);
        System.out.println("Se testeaza getRadius()");
        System.out.println("c1:"+c1.getRadius());
        System.out.println("c2:"+c2.getRadius());
        System.out.println("c3:"+c3.getRadius());
        c1.setRadius(2);
        c2.setRadius(3);
        c3.setRadius(4);
        System.out.println("c1:"+c1.getRadius());
        System.out.println("c2:"+c2.getRadius());
        System.out.println("c3:"+c3.getRadius());
        System.out.println("Se testeaza getArea()");
        System.out.println("c1:"+c1.getArea());
        System.out.println("c2:"+c2.getArea());
        System.out.println("c3:"+c3.getArea());
        System.out.println("Se testeaza getPerimeter()");
        System.out.println("c1:"+c1.getPerimeter());
        System.out.println("c2:"+c2.getPerimeter());
        System.out.println("c3:"+c3.getPerimeter());
        System.out.println("Se testeaza toString()");
        System.out.println(c1.toString());
        System.out.println(c2.toString());
        System.out.println(c3.toString());
    }
    private static void testRectangle()
    {
        Rectangle r1=new Rectangle(),r2=new Rectangle(2,2),r3=new Rectangle(3,3,"Black",false);
        System.out.println("r1.getWidth()="+r1.getWidth());
        r1.setWidth(2);
        System.out.println("r1.getWidth()="+r1.getWidth());
        System.out.println("r1.getLength()="+r1.getLength());
        r1.setLength(2);
        System.out.println("r1.getLength()="+r1.getLength());
        System.out.println("r2.getArea()="+r2.getArea());
        System.out.println("r3.getPerimeter()="+r3.getPerimeter());
        r2.setColor("gigel");
        r1.setFilled(false);
        System.out.println(r1.toString());
        System.out.println(r2.toString());
        System.out.println(r3.toString());

    }
    private static void testSquare()
    {   Scanner s=new Scanner(System.in);
        Square s1=new Square(),s2=new Square(2),s3=new Square(3,"Yellow",false);
        System.out.println("Latura patratului s2 este "+s2.getSide());
        System.out.println("Schimbati lungimea laturii patratului s1");
        int n=s.nextInt();
        s1.setSide(n);
        System.out.println("Latura patratului s1 este "+s1.getSide());
        System.out.println("schimbati culoarea patraturlui s3 si starea sa!");
        String color=s.next();
        boolean filled=s.nextBoolean();
        s3.setFilled(filled);
        s3.setColor(color);
        System.out.println(s3.toString());
        System.out.println("Aria si perimetrul patratului s3 sunt: "+s3.getArea()+", respectiv "+s3.getPerimeter());
    }
    public static void main(String[] args)
    {
        testCircle();
        System.out.println("");
        testRectangle();
        testSquare();
    }
}
