package Ex6;

public class Square extends Rectangle {
    public Square()
    {

    }
    public Square(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public Square(double side, String color,Boolean filled)
    {
        this.setLength(side);
        this.setWidth(side);
        this.setColor(color);
        this.setFilled(filled);
    }
    public double getSide()
    {
        return this.getLength();
    }
    public void setSide(double side)
    {
        this.setLength(side);
        this.setWidth(side);
    }
    public String toString()
    {
        return "A Square with side = "+this.getSide()+",which is a subclass of "+super.toString();
    }
}
