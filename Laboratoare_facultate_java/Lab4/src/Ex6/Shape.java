package Ex6;

public class Shape {
    private String color="red";
    private Boolean filled=true;
    public Shape()
    {

    }
    public Shape(String color,Boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    public String getColor()
    {
        return this.color;
    }
    public void setColor(String color)
    {
        this.color=color;
        System.out.println("Culoarea a fost schimbata in "+this.color);
    }
    public Boolean isFilled()
    {
        return this.filled;
    }
    public void setFilled(Boolean filled)
    {
        this.filled=filled;
        System.out.println("Starea este acum: "+this.filled);
    }
    public String toString()
    {
        if(this.filled)
            return "A Shape with color of "+this.color+" and filled";
        else
            return "A Shape with color of "+this.color+" and Not filled";
    }
}
