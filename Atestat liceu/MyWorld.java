import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
public class MyWorld extends World
{
static Counter counter= new Counter();
static CounterHp counterhp= new CounterHp();
    public MyWorld()
    {    
        super(1000, 600, 1);
        randomBmbs();
        randomUrs();
        randomPingu();
        randomPizza();
        prepare();
        addObject(new boy(),500,590);
        counter.setValue(0);
        counterhp.setValue(10);
        }
    public void randomBmbs ()
    {   int x = Greenfoot.getRandomNumber(1000);
        int y = Greenfoot.getRandomNumber(600);
        addObject(new bmb(),x,y);
    }    
    public void randomUrs ()
    {   int x = Greenfoot.getRandomNumber(1000);
        int y = Greenfoot.getRandomNumber(600);
        addObject(new urs(),x,y);
    }
    public void randomPingu()
    {   int x = Greenfoot.getRandomNumber(1000);
        int y = Greenfoot.getRandomNumber(600);
        addObject(new pingu(),x,y);
    }
    private void prepare()
    {   showText("Score",63,16);
        addObject(counter,63,36);
        showText("HP",937,16);
        addObject(counterhp,937,36);
    }
    public void randomPizza()
    {
        int x = Greenfoot.getRandomNumber(1000);
        int y = Greenfoot.getRandomNumber(600);
        addObject(new HpInc(),x,y);
    }
}


