import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Play here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Play extends Actor
{
    public Play()
    {
        GreenfootImage start = getImage();
        start.setFont(new Font("Helvetica", false,false, 16));
        start.drawString("Play", 9, 35);
        setImage(start);
    }
    public void act() 
    {
        if (Greenfoot.mouseClicked(this))
        Greenfoot.setWorld(new MyWorld());
    }
}
