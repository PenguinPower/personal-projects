import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameOver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameOver extends World
{

    /**
     * Constructor for objects of class GameOver.
     * 
     */
    public GameOver()
    {   
        super(400, 600, 1);
        showText("Game Over",200,100);
        showText("Back to main menu",200,340);
        addObject(new reload(),200,300);
    }
}
