import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class pingu extends Actor
{   private int OK;
    private boolean hidden_bomb=false;
    private static final int bomb=9;
    public pingu()
    {
      if(Greenfoot.getRandomNumber(15)==bomb)
      this.hidden_bomb=true;
    }
    public void act() 
    {
       move(2);
        IfTouching();
    }  
    public void IfTouching()
    {if(isTouching(boy.class))
        {getWorld().addObject(new pingu(),Greenfoot.getRandomNumber(1000),Greenfoot.getRandomNumber(600));
            getWorld().removeObject(this);
            MyWorld.counter.add(1);
            OK=0;
            if(hidden_bomb==true)
            MyWorld.counterhp.add(-1);
        }
        else
        if(isAtEdge())
        {   getWorld().addObject(new pingu(),Greenfoot.getRandomNumber(1000),Greenfoot.getRandomNumber(600));
            getWorld().removeObject(this);
            OK=0;
        }
        if(OK==0)
        {turn(Greenfoot.getRandomNumber(360));
            OK=1;
        }
    }
}
