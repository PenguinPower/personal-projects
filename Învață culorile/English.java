import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class English here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class English extends World
{

    /**
     * Constructor for objects of class English.
     * 
     */
    public English()
    {    
       super(600, 400, 1);
        maro maro = new maro();
        turc turc = new turc();
        bej bej = new bej();
            
        addObject(maro, 160, 90);
        showText("Brown",210,115);
        addObject(turc, 440, 90);
        showText("Turqoise",490,115);
        addObject(bej, 300, 280);
        showText("Beige",350,305);
        addObject(new NextEn(), 560, 360);
        showText("Press left arrow",155,175);
        showText("Press right arrow", 435,175);
        showText("Press down arrow", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Turqoise.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_024.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_017.wav");
            }
        }
}
