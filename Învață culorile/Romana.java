import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Romana extends World
{
    private Actor Next;
    public Romana()
    {    
        super(600, 400, 1);
        maro maro = new maro();
        turc turc = new turc();
        bej bej = new bej();
            
        addObject(maro, 160, 90);
        showText("Maro",210,115);
        addObject(turc, 440, 90);
        showText("Turcoaz",490,115);
        addObject(bej, 300, 280);
        showText("Bej",350,305);
        addObject(new Next(), 560, 360);
        showText("Apăsați săgeată stânga",155,175);
        showText("Apăsați săgeată dreapta", 435,175);
        showText("Apăsați săgeată jos", 320, 365);
       
    }
    public void act()
        {
            if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_001.wav");
            }
            if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_008.wav");
            }
            if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_002.wav");
            }
        }
}
