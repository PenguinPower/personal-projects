import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Romana2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Romana2 extends World
{
private Actor Next;
    /**
     * Constructor for objects of class Romana2.
     * 
     */
    public Romana2()
    {    
        super(600, 400, 1); 
        auriu auriu = new auriu();
        violet violet = new violet();
        rosu rosu = new rosu();
            
        addObject(auriu, 160, 90);
        showText("Auriu",210,115);
        addObject(violet, 440, 90);
        showText("Violet",490,115);
        addObject(rosu, 300, 280);
        showText("Roșu",350,305);
        showText("Apăsați săgeată stânga",155,175);
        showText("Apăsați săgeată dreapta", 435,175);
        showText("Apăsați săgeată jos", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_011.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_009.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_012.wav");
            }
    }
}
