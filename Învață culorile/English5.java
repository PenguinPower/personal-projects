import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class English5 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class English5 extends World
{
    public English5()
    {    
     super(600, 400, 1); 
     liliac liliac = new liliac();
     roz roz = new roz();
     portocaliu portocaliu = new portocaliu();
            
     addObject(liliac, 160, 90);
     showText("Lilac",210,115);
     addObject(roz, 440, 90);
     showText("Pink",490,115);
     addObject(portocaliu, 300, 280);
     showText("Orange",350,305);
     showText("Press left arrow",155,175);
     showText("Press right arrow", 435,175);
     showText("Press down arrow", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_022.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Lilac.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_023.wav");
            }
        }
}
