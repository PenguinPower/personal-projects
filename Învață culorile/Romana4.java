import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Romana4 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Romana4 extends World
{
    private Actor Next;
    /**
     * Constructor for objects of class Romana4.
     * 
     */
    public Romana4()
    {    
        super(600, 400, 1); 
        mov mov = new mov();
        lavanda lavanda = new lavanda();
        crem crem = new crem();
            
        addObject(mov, 160, 90);
        showText("Mov",210,115);
        addObject(lavanda, 440, 90);
        showText("Lavandă",490,115);
        addObject(crem, 300, 280);
        showText("Crem",350,305);
        showText("Apăsați săgeată stânga",155,175);
        showText("Apăsați săgeată dreapta", 435,175);
        showText("Apăsați săgeată jos", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_004.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_003.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_015.wav");
            }
        }
}
