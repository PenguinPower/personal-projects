import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class English4 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class English4 extends World
{

    /**
     * Constructor for objects of class English4.
     * 
     */
    public English4()
    {    
        super(600, 400, 1); 
        mov mov = new mov();
        lavanda lavanda = new lavanda();
        crem crem = new crem();
            
        addObject(mov, 160, 90);
        showText("Purple",210,115);
        addObject(lavanda, 440, 90);
        showText("Lavender",490,115);
        addObject(crem, 300, 280);
        showText("Cream",350,305);
        showText("Press left arrow",155,175);
        showText("Press right arrow", 435,175);
        showText("Press down arrow", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_019.wav");
            }
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Lavender.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_030.wav");
            }
        }
}
