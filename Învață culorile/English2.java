import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class English2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class English2 extends World
{

    /**
     * Constructor for objects of class English2.
     * 
     */
    public English2()
    {    
        super(600, 400, 1); 
        auriu auriu = new auriu();
        violet violet = new violet();
        rosu rosu = new rosu();
            
        addObject(auriu, 160, 90);
        showText("Golden Yellow",190,115);
        addObject(violet, 440, 90);
        showText("Violet",490,115);
        addObject(rosu, 300, 280);
        showText("Red",350,305);
        showText("Press left arrow",155,175);
        showText("Press right arrow", 435,175);
        showText("Press down arrow", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_026.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Voce_025.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_027.wav");
            }
        }
}
