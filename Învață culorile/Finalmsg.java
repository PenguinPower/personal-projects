import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Finalmsg here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Finalmsg extends Actor
{
    /**
     * Act - do whatever the Finalmsg wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Finalmsg()
    {
        String text = "Felicitări! Ai terminat acest curs!";
        /*GreenfootImage img= new greenfoot.GreenfootImage(300,200);
        img.drawString(text,30,20);
        setImage(img);*/
        setImage(new GreenfootImage(text, 30, Color.GREEN, new Color(0,0,0,0)));
    }   
}
