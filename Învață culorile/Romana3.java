import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Romana3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Romana3 extends World
{
private Actor Next;
    /**
     * Constructor for objects of class Romana3.
     * 
     */
    public Romana3()
    {    
        super(600, 400, 1); 
        verde verde = new verde();
        albastru albastru = new albastru();
        galben galben = new galben();
            
        addObject(verde, 160, 90);
        showText("Verde",210,115);
        addObject(albastru, 440, 90);
        showText("Albastru",490,115);
        addObject(galben, 300, 280);
        showText("Galben",350,305);
        showText("Apăsați săgeată stânga",155,175);
        showText("Apăsați săgeată dreapta", 435,175);
        showText("Apăsați săgeată jos", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Voce_013.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Verde.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_014.wav");
            }
        }
}
