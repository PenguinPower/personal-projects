import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class English3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class English3 extends World
{

    /**
     * Constructor for objects of class English3.
     * 
     */
    public English3()
    {    
        super(600, 400, 1); 
        verde verde = new verde();
        albastru albastru = new albastru();
        galben galben = new galben();
            
        addObject(verde, 160, 90);
        showText("Green",210,115);
        addObject(albastru, 440, 90);
        showText("Blue",490,115);
        addObject(galben, 300, 280);
        showText("Yellow",350,305);
        showText("Press left arrow",155,175);
        showText("Press right arrow", 435,175);
        showText("Press down arrow", 320, 365);
    }
    public void act()
    {
        if(Greenfoot.isKeyDown("right"))
            {
                Greenfoot.playSound("Blue.wav");
            }
        if(Greenfoot.isKeyDown("left"))
            {
                Greenfoot.playSound("Green.wav");
            }
        if(Greenfoot.isKeyDown("down"))
            {
                Greenfoot.playSound("Voce_029.wav");
            }
        }
}
