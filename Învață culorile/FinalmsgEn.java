import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FinalmsgEn here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FinalmsgEn extends Actor
{
    /**
     * Act - do whatever the FinalmsgEn wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public FinalmsgEn()
    {
        String text = "Congratulatins! You have finished this course!";
        /*GreenfootImage img= new greenfoot.GreenfootImage(300,200);
        img.drawString(text,30,20);
        setImage(img);*/
        setImage(new GreenfootImage(text, 30, Color.GREEN, new Color(0,0,0,0)));
    }   
}
