import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EnglishFin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EnglishFin extends World
{

    /**
     * Constructor for objects of class EnglishFin.
     * 
     */
    public EnglishFin()
    {    
        super(600, 400, 1); 
        addObject(new Reload(), 230, 200);
        addObject(new Exit(), 340, 200);
        addObject(new FinalmsgEn(), 300, 150); 
    }
}
