import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Start here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Start extends Actor
{   
    public Start()
    {
        GreenfootImage start = getImage();
        start.setFont(new Font("Helvetica", false,false, 16));
        start.drawString("Start", 11, 34);
        setImage(start);
    }
    public void act() 
    {
        GreenfootImage start = getImage();
        start.setFont(new Font("Helvetica", false,false, 16));
        start.drawString("Start", 11, 34);
        setImage(start);
        if (Greenfoot.mouseClicked(this)) 
        {  Ro ro = new Ro();
           En en = new En();
            
           getWorld().addObject(ro, 150, 150);
           getWorld().addObject(en, 150, 250);
           getWorld().removeObjects(getWorld().getObjects(Titlu.class));
           getWorld().removeObjects(getWorld().getObjects(Iesire.class));
           getWorld().removeObjects(getWorld().getObjects(Start.class));
        }
  }
}
