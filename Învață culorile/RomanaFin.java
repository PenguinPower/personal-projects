import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RomanaFin here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RomanaFin extends World
{

    /**
     * Constructor for objects of class RomanaFin.
     * 
     */
    public RomanaFin()
    {    
        super(600, 400, 1); 
        addObject(new Reload(), 230, 200);
        addObject(new Exit(), 340, 200);
        addObject(new Finalmsg(), 300, 150);
    }
}
