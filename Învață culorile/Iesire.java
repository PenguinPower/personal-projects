import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Iesire here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Iesire extends Actor
{
    /**
     * Act - do whatever the Iesire wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Iesire()
    {
        GreenfootImage start = getImage();
        start.setFont(new Font("Helvetica", false,false, 16));
        start.drawString("Ieșire", 9, 35);
        setImage(start);
    }
    public void act() 
    {
        GreenfootImage start = getImage();
        start.setFont(new Font("Helvetica", false,false, 16));
        start.drawString("Ieșire", 9, 35);
        setImage(start);
        if (Greenfoot.mouseClicked(this)) 
        {           
            Greenfoot.stop();
            Greenfoot.setWorld(new aMyWorld());
        }
    }    
}
